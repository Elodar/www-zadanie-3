import React, { Component }  from 'react';
import Article from "./Article";

class ViewAll extends Component {
    state =
    {
articles: []
    }
    
    componentDidMount() {
        fetch('http://localhost:8080/api/articles/all')
            .then(response => response.json())
            .then(articles => {
                console.log(articles)
                this.setState({ articles })
            }
                
    
   
);

}
    


    render() {
        return (
            <div>
                
                {this.state.articles.map(article => < Article info={article} />)}
                
            
                
               
                
                
            </div>
        );
    }
}  



export default ViewAll;
