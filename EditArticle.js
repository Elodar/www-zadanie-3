import React, {Component} from 'react';



class EditArticle extends React.Component {

constructor(props) {
    super(props);
    this.state = {
            articleId:'',
            title: '',
            text: ''
    };
  }

  handleSubmit = (event) => {
    event.preventDefault();
     console.log("title "+this.state.title)
     fetch('http://localhost:8080/api/articles/update', {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
             articleId: this.state.articleId,
            title: this.state.title,
            text: this.state.text
        })
        })
        
        
};


render() {
return (
  <div>
      <form onSubmit={this.handleSubmit}>
      <input 
                type="text" 
                placeholder="id"
                maxLength="255"
                value={this.state.articleId}
                onChange={event => this.setState({ articleId: event.target.value })}
                required/>

            <input 
                type="text" 
                placeholder="title"
                maxLength="255"
                style={{marginLeft: "20px"}}
                value={this.state.title}
                onChange={event => this.setState({ title: event.target.value })}
                required/>
            <input 
                type="text" 
                placeholder="text" 
                maxLength="5000"
                style={{marginLeft: "20px"}} 
                value={this.state.text}
                onChange={event => this.setState({ text: event.target.value })}
                required />
         
            
           
            <button  type="submit" className="btn btn-primary  btn-lg" style={{marginLeft: "10px"}} onClick={this.validate}>
                    Edit article
            </button>
           
        </form>
         
         </div>
);
}
}
export default EditArticle;