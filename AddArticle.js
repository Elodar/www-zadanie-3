import React, {Component} from 'react';



class AddArticle extends React.Component {

constructor(props) {
    super(props);
    this.state = {
            title: '',
            text: ''
    };
  }

  handleSubmit = (event) => {
    event.preventDefault();
     console.log("title "+this.state.title)
     fetch('http://localhost:8080/api/articles/add', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            title: this.state.title,
            text: this.state.text
        })
        })
        
        
};


render() {
return (
  <div>
      <form onSubmit={this.handleSubmit}>
            <input 
                type="text" 
                placeholder="title"
                maxLength="255"
                value={this.state.title}
                onChange={event => this.setState({ title: event.target.value })}
                required/>
            <input 
                type="text" 
                placeholder="text" 
                maxLength="5000"
                style={{marginLeft: "20px"}} 
                value={this.state.text}
                onChange={event => this.setState({ text: event.target.value })}
                required />
         
            
           
            <button  type="submit" className="btn btn-warning  btn-lg" style={{marginLeft: "20px"}} onClick={this.validate}>
                    Add article
            </button>
           
        </form>
         
         </div>
);
}
}
export default AddArticle;