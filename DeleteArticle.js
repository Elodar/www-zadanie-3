import React, {Component} from 'react';



class DeleteArticle extends React.Component {

constructor(props) {
    super(props);
    this.state = {
            articleId:'',
            
    };
  }

  handleSubmit = (event) => {
    event.preventDefault();
     console.log("title "+this.state.title)
     fetch('http://localhost:8080/api/articles/delete', {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
             articleId: this.state.articleId,
           
        })
        })
        
        
};


render() {
return (
  <div>
      <form onSubmit={this.handleSubmit}>
      <input 
                type="text" 
                placeholder="id"
                maxLength="255"
                value={this.state.articleId}
                onChange={event => this.setState({ articleId: event.target.value })}
                required/>
                    
            <button  type="submit" className="btn btn-primary  btn-lg" style={{marginLeft: "10px"}} onClick={this.validate}>
                    Edit article
            </button>
           
        </form>
         
         </div>
);
}
}
export default DeleteArticle;